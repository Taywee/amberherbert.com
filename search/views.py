from amber import models
from django.core.paginator import Paginator
from django.template.response import TemplateResponse

from wagtail.models import Page
from wagtail.search.models import Query


def search(request):
    search_query = request.GET.get('query', None)
    page = request.GET.get('page', 1)

    # Search
    if search_query:
        search_results = Page.objects.live().reverse().search(search_query)
        query = Query.get(search_query)

        # Record hit
        query.add_hit()
    else:
        search_results = Page.objects.none()

    # Pagination
    paginator = Paginator(search_results, 10)

    search_results = paginator.page(page)

    page_range = (
            page if isinstance(page, int) else None
            for page in paginator.get_elided_page_range(search_results.number, on_each_side=1, on_ends=2)
        )

    site = models.Home.objects.live().first()

    return TemplateResponse(request, 'search/search.html', {
        'search_query': search_query,
        'search_results': search_results,
        'page_range': page_range,
        'self': site,
    })
