# TT2020

The header font is taken from https://ctrlcctrlv.github.io/TT2020/ , released
under the SIL Open Font License: https://github.com/ctrlcctrlv/TT2020/blob/master/LICENSE
Copyright (c) 2020 Fredrick R. Brennan (<copypaste@kittens.ph>)

# Typewriter header image

The header image is taken from pixabay.com:
https://pixabay.com/photos/vintage-typewriter-old-1148940/

# Swiper

The carousel uses Swiper, which is ©2019 Vladimir Kharlampidi, and provided
under the MIT license.
