# Design

* Home Page
    * Most recent posts
    * Pinned as important
* Writing Journey
    * Place to document career
* Written Works
    * Published work
    * In-progress projects
    * Poetry
    * Short fiction
* Reviews
    * Books
    * TV
    * Movies
* Editing/Writing tips
* About Me
    * Bio
    * Photo
    * Links to social media
    * Cirriculum Vitae
* Etc
    * Search functionality
    * Comment sections
    * Newsletter/email sign-up
    * RSS

Dev notes:

* Home page will need to have a way of aggregating recent posts from other
  categories.  Probably just taking the most recent pages of a particular
  category, such as a common base class (with a date and summary).  The count
  should be configurable.
* Writing Journey needs to be some sort of blog type, with properly-enumerable
  posts and traversible archives (archives probably as a separate page).  A
  calendar picker might be best for that.
* Automatic cross-posting to Medium and Twitter, and maybe Instagram, for some
  things (all the keys should be part of the CMS itself, probably on the root
  site page object).
* Blog, poetry, short fiction, and reviews will all need to be properly
  taggable, and they will also drop into the Home Page recent posts, and will be
  pinnable as important.
* Editing and Writing tips will need some method of organization to make it
  easier to use.  Maybe a simple category system or something.
* Disable Google FLoC globally.
* Globally allow web spiders.
