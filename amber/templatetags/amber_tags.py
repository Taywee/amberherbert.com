#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright © 2021 Taylor C. Richberger
# This code is released under the license described in the LICENSE file

from django import template
from amber import models
from typing import FrozenSet, List, Tuple, Union, Sequence, Iterator, Optional
from wagtail.models import Page
from urllib.parse import urlencode

register = template.Library()

@register.simple_tag(takes_context=True)
def currentpath(context, url) -> bool:
    return context['request'].path.startswith(url)

@register.filter
def is_directory(page: models.Page) -> bool:
    return isinstance(page.specific, models.PageDirectory)

@register.filter
def is_feed(page: models.Page) -> bool:
    return isinstance(page.specific, models.Feed)

@register.filter
def without_tag(tags: FrozenSet, tag: str) -> FrozenSet:
    return tags - {tag}

@register.filter
def with_tag(tags: FrozenSet, tag: str) -> FrozenSet:
    return tags | {tag}

@register.simple_tag
def build_query(**kwargs: Optional[Union[str, Sequence[str], int, float]]) -> str:
    '''Build a query string, with the ?, from kwargs.

    None and blank string arguments are not included, and an empty query string
    gives no question mark, just blank.

    Non-string arguments are treated as a sequence and are iterated over.

    Query parameters are sorted, and sequence values are sorted.
    '''

    query: List[Tuple[str, str]] = []

    for key, value in kwargs.items():
        first_page = key == 'page' and (value == 1 or value == '1')

        if value and not first_page:
            if isinstance(value, str):
                query.append((key, value))
            elif isinstance(value, (int, float)):
                query.append((key, str(value)))
            else:
                query.append((key, ','.join(sorted(value))))

    if query:
        return '?' + urlencode(sorted(query))
    else:
        return ''

@register.simple_tag
def breadcrumbs(page, root: Optional[Page] = None) -> Iterator[Page]:
    if root is None:
        root = page.get_site().root_page

    if page != root:
        yield from breadcrumbs(page.get_parent(), root)

        yield page
