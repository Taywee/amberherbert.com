from django.db import models
from django.utils.timezone import localtime
from django.core.paginator import Paginator
from wagtail.admin.panels import FieldPanel
from wagtail.fields import RichTextField
from wagtail.models import Page
from wagtail.images.models import Image
from wagtail.search.index import SearchField
from modelcluster.fields import ParentalKey
from modelcluster.contrib.taggit import ClusterTaggableManager
from taggit.models import TaggedItemBase

from typing import Set

from .feed import Feed
from .help import cleanhelp

class Blog(Feed, Page):
    '''A section of taggable items sorted and archived by date, with a full
    tagging system.

    This is intended for the Writing Journey, Poetry, and Short Stories
    sections.
    '''

    subpage_types = ['amber.Post']

    def get_context(self, request):
        context = super().get_context(request)

        page_number = request.GET.get('page', 1)
        tags_string = request.GET.get('tags')

        if tags_string:
            tags = frozenset(tags_string.split(','))
        else:
            tags = frozenset()

        children = Post.objects.child_of(self).live().order_by('-created')

        for tag in tags:
            children = children.filter(tags__name=tag)

        # Only fill out available tags if a tag is already selected.
        available_tags: Set[str] = set()
        if tags:
            for child in children:
                for tag in child.tags.all():
                    available_tags.add(tag.name)

        paginator = Paginator(children, 10)

        children_page = paginator.page(page_number)

        context['children_page'] = children_page
        context['page_range'] = (
            page if isinstance(page, int) else None
            for page in paginator.get_elided_page_range(children_page.number, on_each_side=1, on_ends=2)
        )
        context['tags'] = tags
        context['available_tags'] = available_tags

        return context

class PostTag(TaggedItemBase):
    content_object = ParentalKey('amber.Post', on_delete=models.CASCADE, related_name='tagged_items')

class Post(Page):
    '''A page in a blog section.
    '''

    created = models.DateTimeField(
        default=localtime,
    )

    featured = models.BooleanField(
        default=False,
        help_text=cleanhelp(
            '''
            Whether this post is linked to the "Featured" section on the home
            page.  Items in "Latest" won't appear in the "Featured" section.
            '''
        ),
    )

    body = RichTextField()

    image = models.ForeignKey(
        Image,
        blank=True,
        null=True,
        default=None,
        on_delete=models.SET_NULL,
        help_text=cleanhelp(
            '''
            The main image to associate with this post.  Posts may have other
            images in the body, but this is used as the background for the
            "Featured" section and also as the image when this page is linked
            externally on third-party sites.
            '''
        ),
    )

    tags = ClusterTaggableManager(through=PostTag, blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('created'),
        FieldPanel('featured'),
        FieldPanel('body', classname='full'),
        FieldPanel('image'),
    ]

    promote_panels = Page.promote_panels + [
        FieldPanel('tags'), 
    ] # type: ignore

    search_fields = Page.search_fields + [
        SearchField('body'),
    ]

    subpage_types = []
