from wagtail.admin.panels import FieldPanel
from wagtail.fields import RichTextField
from wagtail.models import Page
from wagtail.search.index import SearchField

from .feed import Feed

class PageDirectory(Feed, Page):
    '''A page as reachable from the home page that just holds other pages, for a drop-down navigation.
    '''

    parent_page_types = ['amber.Home']
    subpage_types = ['amber.RichPage', 'amber.Blog', 'amber.ReviewSection']

class RichPage(Page):
    '''A rich top-level page type, intended for full pages that are navigable
    in an obvious way.
    '''

    body = RichTextField()

    content_panels = Page.content_panels + [
        FieldPanel('body', classname='full'),
    ]

    search_fields = Page.search_fields + [
        SearchField('body'),
    ]

    subpage_types = []
