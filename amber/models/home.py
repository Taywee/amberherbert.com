from django.db import models
from wagtail.admin.panels import FieldPanel
from wagtail.models import Page
from wagtail.fields import StreamField
from wagtail import blocks
from wagtail.images.models import Image

from .feed import Feed

class SocialLinkBlock(blocks.StructBlock):
    class_name = blocks.CharBlock(
        help='The class name used to give this link an icon',
    )
    url = blocks.URLBlock(
        help='The URL of the link',
    )

    class Meta:
        icon = 'link'
        template = 'amber/_social-link.html'

class Home(Feed, Page):
    '''The base site page.

    This represents the Home page, mostly, and also controls the header and
    navigation bits.
    '''

    social_links = StreamField(
        [
            ('link', SocialLinkBlock()),
        ],
        blank=True,
        use_json_field=True,
    )

    logo_image = models.ForeignKey(
        Image,
        blank=True,
        null=True,
        default=None,
        on_delete=models.SET_NULL,
    )

    content_panels = Page.content_panels + [
        FieldPanel('social_links'),
        FieldPanel('logo_image'),
    ]

    subpage_types = [
        'amber.PageDirectory',
        'amber.RichPage',
        'amber.Blog',
        'amber.ReviewSection',
        'amber.MailingList',
    ]
