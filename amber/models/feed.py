from django.conf import settings
from django.utils.feedgenerator import Rss201rev2Feed, Atom1Feed, SyndicationFeed
from django.http import HttpRequest, HttpResponse
from functools import reduce
from wagtail.models import Page
from wagtail.rich_text import expand_db_html

from typing import Optional, Union, Generator, Mapping, Type, Optional, Iterable

from amber import models

class Feed:
    '''A special mixin that imbues its including model with a feed.
    '''

    __FORMAT_MAP: Mapping[Union[str, None], str] = {
        'rss': 'application/rss+xml',
        'atom': 'application/atom+xml',
    }

    __GENERATOR_MAP: Mapping[Union[str, None], Type[SyndicationFeed]] = {
        'application/rss+xml': Rss201rev2Feed,
        'application/atom+xml': Atom1Feed,
    }

    def serve(self, request: HttpRequest) -> HttpResponse:
        feed_type: Optional[str] = request.GET.get('feed') # type: ignore
        format: Optional[str] = self.__FORMAT_MAP.get(feed_type)
        generator: Optional[Type[SyndicationFeed]] = self.__GENERATOR_MAP.get(format)
        if generator is None:
            return super().serve(request) # type: ignore

        site_title: str = self.get_site().site_name # type: ignore
        if site_title == self.title: # type: ignore
            title = site_title
        else:
            title = f'{self.title} - {site_title}' # type: ignore

        feed = generator(
            title=title,
            link=self.get_full_url(), #type: ignore
            description=self.search_description, #type: ignore
            subtitle=self.search_description, #type: ignore
            language=settings.LANGUAGE_CODE, #type: ignore
            feed_url=self.get_full_url(), #type: ignore
            feed_guid=self.get_full_url(), #type: ignore
        )

        posts: Iterable[models.blog.Post] = reduce(
            lambda qs, blog: models.blog.Post.objects.child_of(blog) | qs,
            Feed.__blogs(self), # type: ignore
            models.blog.Post.objects.none(),
        ).order_by('-created')[:10]

        for post in posts:
            owner = post.owner
            feed.add_item(
                title=post.title,
                link=post.get_full_url(),
                description=(post.search_description or '').strip() or expand_db_html(post.body),
                author_email=owner.email, # type: ignore
                author_name=f'{owner.first_name} {owner.last_name}', # type: ignore
                pubdate=post.created,
                unique_id=post.get_full_url(),
            )

        return HttpResponse(
            feed.writeString('utf-8'),
            content_type=format,
        )

    @staticmethod
    def __blogs(page: Page) -> Generator['models.blog.Blog', None, None]:
        '''Yield all Blogs in this page tree, including the tree itself.'''
        if page.live:
            if isinstance(page, models.blog.Blog):
                yield page

            for child in page.get_children().specific():
                yield from Feed.__blogs(child)
