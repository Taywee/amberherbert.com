from django.db import models
from wagtail.admin.panels import FieldPanel
from wagtail.models import Page

class MailingList(Page):
    '''A Mailing list.

    Embeds a SendGrid signup iframe
    '''

    form_url = models.URLField(
        blank=False,
        null=False,
        help_text="The SendGrid signup form URL",
    )

    content_panels = Page.content_panels + [
        FieldPanel('form_url'),
    ]

    subpage_types = [
    ]
