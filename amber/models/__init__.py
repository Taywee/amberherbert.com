from .home import *
from .pages import *
from .blog import *
from .review import *
from .mailing_list import *
