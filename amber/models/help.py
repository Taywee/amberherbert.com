from inspect import cleandoc
import re

_SPLITTER = re.compile(r'\n{2,}')

def cleanhelp(text: str) -> str:
    '''Unwrap and unindent the given text, similar to docstring handling.

    Newlines are replaced with spaces, and double (or more) newlines are
    replaced with newlines, similar to markdown handling.
    '''

    dedented = cleandoc(text)
    lines = _SPLITTER.split(dedented)
    return '\n'.join(line.replace('\n', ' ') for line in lines)

