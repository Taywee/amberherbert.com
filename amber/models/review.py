from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from wagtail.admin.panels import FieldPanel
from wagtail.search.index import SearchField

from decimal import Decimal

from .blog import Blog, Post
from .help import cleanhelp

class ReviewSection(Blog):
    '''A section for reviews, allowing for review-specific behavior and styling.
    '''

    subpage_types = ['amber.Review']

    template = 'amber/blog.html'

class Review(Post):
    '''A reviewed item.
    '''

    subject = models.CharField(
        default='',
        max_length=256,
        help_text=cleanhelp(
            '''
            The title of the item being reviewed, such as a book or film title.
            '''
        ),
    )

    score = models.DecimalField(
        help_text='The review score, from 0.0 to 5.0, with one allowed decimal place.',
        null=True,
        default=None,
        blank=True,
        max_digits=2,
        decimal_places=1,
        validators=[
            MinValueValidator(Decimal('0.0')),
            MaxValueValidator(Decimal('5.0')),
        ],
    )

    uri = models.TextField(
        default='',
        blank=True,
        help_text=cleanhelp(
            '''
            An optional unique-identifying URI for a reviewed item.  Books
            could refer to a GoodReads URL, or use a urn:isbn: prefixed ISBN
            URN.  Movies or television should probably refer to IMDB.
            '''
        ),
    )

    content_panels = [
        FieldPanel('subject'),
        FieldPanel('uri'),
    ] + Post.content_panels

    search_fields = [
        SearchField('subject'),
        SearchField('uri'),
    ] + Post.search_fields

    parent_page_types = ['amber.ReviewSection']

    template = 'amber/post.html'
