class DisableFLoC:
    '''Simply add the header to globally opt out of Google's FLoC.
    '''

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        response['Permissions-Policy'] = "interest-cohort=()"
        return response
