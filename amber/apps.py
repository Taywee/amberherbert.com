#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright © 2021 Taylor C. Richberger
# This code is released under the license described in the LICENSE file

from django.apps import AppConfig

class AmberConfig(AppConfig):
    name = 'amber'
    verbose_name = 'Amber Herbert'
