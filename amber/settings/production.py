from .base import *
from os import environ
import toml

with open(environ.get('AMBERHERBERT_CONFIG', '/etc/amberherbert/config.toml')) as file:
    config = toml.load(file)

DEBUG = False

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': config['database']['name'],
        'USER': config['database']['user'],
        'PASSWORD': config['database']['password'],
        'HOST': config['database']['host'],
        'PORT': config['database']['port'],
    }
}

SECRET_KEY = config['site']['secret-key']
ALLOWED_HOSTS = ['amberherbert.com']

WAGTAILSEARCH_BACKENDS = {
    'default': {
        'BACKEND': 'wagtail.search.backends.database',
    }
}

WAGTAIL_ADMIN_PATH = config['site']['wagtail-admin-path']
DJANGO_ADMIN_PATH = config['site']['django-admin-path']

STATIC_ROOT = config['files']['static']
MEDIA_ROOT = config['files']['media']

STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.ManifestStaticFilesStorage'
