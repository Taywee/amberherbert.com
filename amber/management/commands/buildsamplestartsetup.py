#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright © 2021 Taylor C. Richberger
# This code is released under the license described in the LICENSE file

from decimal import Decimal
from django.db import transaction
from datetime import datetime, timedelta
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand
from amber import models
from wagtail.models import Page, Site
import random
import logging

logger = logging.getLogger(__name__)

class Command(BaseCommand):
    help = 'Create start set of data'

    def handle(self, *args, **options) -> None:
        with transaction.atomic():
            start = datetime.now().astimezone() - timedelta(days=365)

            logger.info('Creating dev user')

            User = get_user_model()
            user = User.objects.create_superuser('dev', 'dev@dev.dev', 'dev', first_name='Amber', last_name='Herbert')

            root = Page.get_first_root_node()

            if root is None:
                raise RuntimeError('Can not find root node')

            for child in root.get_children():
                child.delete()

            root = Page.get_first_root_node()

            logger.info('Creating Home page')

            home = models.Home(
                title='Amber Herbert',
                draft_title='Amber Herbert',
                search_description="Amber Herbert's web site",
                owner=user,
            )

            root.add_child(instance=home)

            Site.objects.create(
                hostname='amberherbert.localhost',
                port=8000,
                site_name='Amber Herbert',
                root_page=home,
                is_default_site=True,
            )

            logger.info('Creating Writing Journey')

            writing_journey = models.Blog(
                title='Writing Journey',
                draft_title='Writing Journey',
                search_description='My journey as an author',
                show_in_menus=True,
                owner=user,
            )

            home.add_child(instance=writing_journey)

            for i in range(30):
                post = models.Post(
                    title=f'Writing Journey post {i} title',
                    draft_title=f'Writing Journey post {i} title',
                    created=start + timedelta(days=1) + timedelta(days=10) * i,
                    search_description=f'Writing Journey post {i} description',
                    body=f'<p>Writing Journey post {i} body</p>{lorem_ipsum}',
                    featured=(i == 5),
                    owner=user,
                )

                writing_journey.add_child(instance=post)

                if i % 2 == 0:
                    post.tags.add('blog_even')
                else:
                    post.tags.add('blog_odd')
                post.save()

            logger.info('Creating Written Works')

            written_works = models.PageDirectory(
                title='Written Works',
                draft_title='Written Works',
                search_description='My works',
                show_in_menus=True,
                owner=user,
            )

            home.add_child(instance=written_works)
            
            published_work = models.RichPage(
                title='Published Works',
                draft_title='Published Works',
                search_description='My published works',
                show_in_menus=True,
                body=f'<p><i>My published book</i> by Amber Richberger</p>{lorem_ipsum}',
                owner=user,
            )
            in_progress_work = models.RichPage(
                title='In-Progress Works',
                draft_title='In-Progress Works',
                search_description='My in-Progress works',
                show_in_menus=True,
                body=f'<p><i>My unpublished book</i> by Amber Richberger</p>{lorem_ipsum}',
                owner=user,
            )

            poetry = models.Blog(
                title='Poetry',
                draft_title='Poetry',
                search_description='My published poetry',
                show_in_menus=True,
                owner=user,
            )

            short_fiction = models.Blog(
                title='Short Fiction',
                draft_title='Short Fiction',
                search_description='My published short fictions',
                show_in_menus=True,
                owner=user,
            )

            written_works.add_child(instance=published_work)
            written_works.add_child(instance=in_progress_work)
            written_works.add_child(instance=poetry)
            written_works.add_child(instance=short_fiction)

            for i in range(30):
                post = models.Post(
                    title=f'Poem {i} title',
                    draft_title=f'Poem {i} title',
                    created=start + timedelta(days=2) + timedelta(days=10) * i,
                    search_description=f'Poem {i} description',
                    body=f'<p>Poem {i} body</p>{lorem_ipsum}',
                    featured=(i == 6),
                    owner=user,
                )

                poetry.add_child(instance=post)

                if i % 2 == 0:
                    post.tags.add('poetry_even')
                else:
                    post.tags.add('poetry_odd')
                post.save()

            for i in range(30):
                post = models.Post(
                    title=f'Short fiction {i} title',
                    draft_title=f'Short fiction {i} title',
                    created=start + timedelta(days=3) + timedelta(days=10) * i,
                    search_description=f'Short fiction {i} description',
                    body=f'<p>Short fiction {i} body</p>{lorem_ipsum}',
                    featured=(i == 7),
                    owner=user,
                )

                short_fiction.add_child(instance=post)

                if i % 2 == 0:
                    post.tags.add('short_fiction_even')
                else:
                    post.tags.add('short_fiction_odd')

                post.save()

            logger.info('Creating Reviews')

            reviews = models.PageDirectory(
                title='Reviews',
                draft_title='Reviews',
                search_description='My reviews',
                show_in_menus=True,
                owner=user,
            )

            home.add_child(instance=reviews)

            book_reviews = models.ReviewSection(
                title='Books',
                draft_title='Books',
                search_description='My published book reviews',
                show_in_menus=True,
                owner=user,
            )

            television_reviews = models.ReviewSection(
                title='Television',
                draft_title='Television',
                search_description='My published television reviews',
                show_in_menus=True,
                owner=user,
            )

            movie_reviews = models.ReviewSection(
                title='Movies',
                draft_title='Movies',
                search_description='My published movie reviews',
                show_in_menus=True,
                owner=user,
            )

            reviews.add_child(instance=book_reviews)
            reviews.add_child(instance=television_reviews)
            reviews.add_child(instance=movie_reviews)

            for i in range(30):
                post = models.Review(
                    subject=f'Book {i}',
                    title=f'Book review {i} title',
                    draft_title=f'Book review {i} title',
                    created=start + timedelta(days=4) + timedelta(days=10) * i,
                    search_description=f'Book review {i} description',
                    body=f'<p>Book review {i} body</p>{lorem_ipsum}',
                    score=Decimal(random.randint(0, 50)) / 10,
                    featured=(i == 8 or i == 9),
                owner=user,
                )

                book_reviews.add_child(instance=post)

                if i % 2 == 0:
                    post.tags.add('book_review_even')
                else:
                    post.tags.add('book_review_odd')

                post.save()

            for i in range(30):
                post = models.Review(
                    subject=f'Television {i}',
                    title=f'Television review {i} title',
                    draft_title=f'Television review {i} title',
                    created=start + timedelta(days=5) + timedelta(days=10) * i,
                    search_description=f'Television review {i} description',
                    body=f'<p>Television review {i} body</p>{lorem_ipsum}',
                    score=Decimal(random.randint(0, 50)) / 10,
                    featured=(i == 10),
                    owner=user,
                )

                television_reviews.add_child(instance=post)

                if i % 2 == 0:
                    post.tags.add('television_review_even')
                else:
                    post.tags.add('television_review_odd')

                post.save()

            for i in range(30):
                post = models.Review(
                    subject=f'Movie {i}',
                    title=f'Movie review {i} title',
                    draft_title=f'Movie review {i} title',
                    created=start + timedelta(days=6) + timedelta(days=10) * i,
                    search_description=f'Movie review {i} description',
                    body=f'<p>Movie review {i} body</p>{lorem_ipsum}',
                    score=Decimal(random.randint(0, 50)) / 10,
                    owner=user,
                )

                movie_reviews.add_child(instance=post)

                if i % 2 == 0:
                    post.tags.add('movie_review_even')
                else:
                    post.tags.add('movie_review_odd')

                post.save()

            about = models.RichPage(
                title='About Me',
                draft_title='About Me',
                search_description='This is a description of me',
                show_in_menus=True,
                body=f'<p>My name is Amber, and this page is all about me</p>{lorem_ipsum}',
                owner=user,
            )

            home.add_child(instance=about)

            logger.info('Creating Writing Tips')

            writing_tips = models.Blog(
                title='Writing Tips',
                draft_title='Writing Tips',
                search_description='These are tips I give about writing',
                show_in_menus=True,
            )

            home.add_child(instance=writing_tips)

            for i in range(30):
                post = models.Post(
                    title=f'Writing Tips post {i} title',
                    draft_title=f'Writing Tips post {i} title',
                    created=start + timedelta(days=7) + timedelta(days=10) * i,
                    search_description=f'Writing Tips post {i} description',
                    body=f'<p>Writing Tips post {i} body</p>{lorem_ipsum}',
                    featured=(i == 11),
                    owner=user,
                )

                writing_tips.add_child(instance=post)

                if i % 2 == 0:
                    post.tags.add('writing_tips_even')
                else:
                    post.tags.add('writing_tips_odd')
                post.save()

lorem_ipsum = '''
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. <b>Tubulo putas dicere?</b> Non quaeritur autem quid naturae tuae consentaneum sit, sed quid disciplinae. <a href='http://loripsum.net/' target='_blank'>Quae contraria sunt his, malane?</a> At ego quem huic anteponam non audeo dicere; Duo Reges: constructio interrete. Recte, inquit, intellegis. Ut non sine causa ex iis memoriae ducta sit disciplina. Unum, cum in voluptate sumus, alterum, cum in dolore, tertium hoc, in quo nunc equidem sum, credo item vos, nec in dolore nec in voluptate; Etsi qui potest intellegi aut cogitari esse aliquod animal, quod se oderit? An quod ita callida est, ut optime possit architectari voluptates? </p>

<p>Materiam vero rerum et copiam apud hos exilem, apud illos uberrimam reperiemus. Commentarios quosdam, inquam, Aristotelios, quos hic sciebam esse, veni ut auferrem, quos legerem, dum essem otiosus; Ex ea difficultate illae fallaciloquae, ut ait Accius, malitiae natae sunt. Quin etiam ferae, inquit Pacuvius, quíbus abest, ad praécavendum intéllegendi astútia, iniecto terrore mortis horrescunt. Nunc ita separantur, ut disiuncta sint, quo nihil potest esse perversius. <b>Id Sextilius factum negabat.</b> </p>

<blockquote cite='http://loripsum.net'>
	Inquit, cum ego te hac nova lege videam eodem die accusatori responderet tribus horis perorare, in hac me causa tempus dilaturum putas?
</blockquote>


<blockquote cite='http://loripsum.net'>
	Si enim sapiens aliquis miser esse possit, ne ego istam gloriosam memorabilemque virtutem non magno aestimandam putem.
</blockquote>


<p>Num igitur utiliorem tibi hunc Triarium putas esse posse, quam si tua sint Puteolis granaria? Ego autem existimo, si honestum esse aliquid ostendero, quod sit ipsum vi sua propter seque expetendum, iacere vestra omnia. Nec vero ut voluptatem expetat, natura movet infantem, sed tantum ut se ipse diligat, ut integrum se salvumque velit. Sed quoniam et advesperascit et mihi ad villam revertendum est, nunc quidem hactenus; Quasi vero aut concedatur in omnibus stultis aeque magna esse vitia, et eadem inbecillitate et inconstantia L. Quae enim dici Latine posse non arbitrabar, ea dicta sunt a te verbis aptis nec minus plane quam dicuntur a Graecis. Est enim effectrix multarum et magnarum voluptatum. Quis enim potest istis, quae te, ut ais, delectant, brevibus et acutis auditis de sententia decedere? <b>Maximus dolor, inquit, brevis est.</b> Quod autem in homine praestantissimum atque optimum est, id deseruit. Crasso, quem semel ait in vita risisse Lucilius, non contigit, ut ea re minus agelastoj ut ait idem, vocaretur. Quis enim potest istis, quae te, ut ais, delectant, brevibus et acutis auditis de sententia decedere? </p>

<ul>
	<li>Quod non faceret, si in voluptate summum bonum poneret.</li>
	<li>Itaque mihi non satis videmini considerare quod iter sit naturae quaeque progressio.</li>
	<li>Minime vero istorum quidem, inquit.</li>
	<li>Neque enim disputari sine reprehensione nec cum iracundia aut pertinacia recte disputari potest.</li>
	<li>Gerendus est mos, modo recte sentiat.</li>
</ul>


<ol>
	<li>Potius inflammat, ut coercendi magis quam dedocendi esse videantur.</li>
	<li>Nihil minus, contraque illa hereditate dives ob eamque rem laetus.</li>
	<li>Nam quibus rebus efficiuntur voluptates, eae non sunt in potestate sapientis.</li>
</ol>


<ul>
	<li>At cum tuis cum disseras, multa sunt audienda etiam de obscenis voluptatibus, de quibus ab Epicuro saepissime dicitur.</li>
	<li>Quae cum essent dicta, discessimus.</li>
	<li>Non semper, inquam;</li>
	<li>Universa enim illorum ratione cum tota vestra confligendum puto.</li>
</ul>


<p>Cui vero in voluptate summum bonum est, huic omnia sensu, non ratione sunt iudicanda, eaque dicenda optima, quae sint suavissima. Ita finis bonorum existit secundum naturam vivere sic affectum, ut optime is affici possit ad naturamque accommodatissime. </p>

<p><a href='http://loripsum.net/' target='_blank'>Confecta res esset.</a> Sin te auctoritas commovebat, nobisne omnibus et Platoni ipsi nescio quem illum anteponebas? Quid igitur dubitamus in tota eius natura quaerere quid sit effectum? Quis est enim, qui hoc cadere in sapientem dicere audeat, ut, si fieri possit, virtutem in perpetuum abiciat, ut dolore omni liberetur? Sed isti ipsi, qui voluptate et dolore omnia metiuntur, nonne clamant sapienti plus semper adesse quod velit quam quod nolit? Epicurus autem cum in prima commendatione voluptatem dixisset, si eam, quam Aristippus, idem tenere debuit ultimum bonorum, quod ille; Ego quoque, inquit, didicerim libentius si quid attuleris, quam te reprehenderim. <b>Aliter enim explicari, quod quaeritur, non potest.</b> <a href='http://loripsum.net/' target='_blank'>Equidem e Cn.</a> Is hoc melior, quam Pyrrho, quod aliquod genus appetendi dedit, deterior quam ceteri, quod penitus a natura recessit. </p>

<p>Is ita vivebat, ut nulla tam exquisita posset inveniri voluptas, qua non abundaret. <i>Nos cum te, M.</i> Quis istud possit, inquit, negare? Sit enim idem caecus, debilis. <i>Ille enim occurrentia nescio quae comminiscebatur;</i> Haec mihi videtur delicatior, ut ita dicam, molliorque ratio, quam virtutis vis gravitasque postulat. <i>Cyrenaici quidem non recusant;</i> De malis autem et bonis ab iis animalibus, quae nondum depravata sint, ait optime iudicari. His similes sunt omnes, qui virtuti student levantur vitiis, levantur erroribus, nisi forte censes Ti. </p>

<p>Cenasti in vita numquam bene, cum omnia in ista Consumis squilla atque acupensere cum decimano. Sit hoc ultimum bonorum, quod nunc a me defenditur; Simus igitur contenti his. Nam constitui virtus nullo modo potesti nisi ea, quae sunt prima naturae, ut ad summam pertinentia tenebit. <mark>Habes, inquam, Cato, formam eorum, de quibus loquor, philosophorum.</mark> Superiores tres erant, quae esse possent, quarum est una sola defensa, eaque vehementer. Itaque et manendi in vita et migrandi ratio omnis iis rebus, quas supra dixi, metienda. </p>

<p><b>Quid de Pythagora?</b> Ergo in utroque exercebantur, eaque disciplina effecit tantam illorum utroque in genere dicendi copiam. <a href='http://loripsum.net/' target='_blank'>Et quod est munus, quod opus sapientiae?</a> <a href='http://loripsum.net/' target='_blank'>Utrum igitur tibi litteram videor an totas paginas commovere?</a> Qui est in parvis malis. <b>At ille pellit, qui permulcet sensum voluptate.</b> Cenasti in vita numquam bene, cum omnia in ista Consumis squilla atque acupensere cum decimano. Qua ex cognitione facilior facta est investigatio rerum occultissimarum. Et quidem, Cato, hanc totam copiam iam Lucullo nostro notam esse oportebit; Est igitur officium eius generis, quod nec in bonis ponatur nec in contrariis. <a href='http://loripsum.net/' target='_blank'>Quasi ego id curem, quid ille aiat aut neget.</a> Universa enim illorum ratione cum tota vestra confligendum puto. </p>

<p>Et hunc idem dico, inquieta sed ad virtutes et ad vitia nihil interesse. Est autem etiam actio quaedam corporis, quae motus et status naturae congruentis tenet; Et harum quidem rerum facilis est et expedita distinctio. Est tamen ea secundum naturam multoque nos ad se expetendam magis hortatur quam superiora omnia. Dicet pro me ipsa virtus nec dubitabit isti vestro beato M. <mark>Omnia contraria, quos etiam insanos esse vultis.</mark> <b>Bonum patria: miserum exilium.</b> Nunc omni virtuti vitium contrario nomine opponitur. Vide ne ista sint Manliana vestra aut maiora etiam, si imperes quod facere non possim. </p>

<ul>
	<li>Ac tamen, ne cui loco non videatur esse responsum, pauca etiam nunc dicam ad reliquam orationem tuam.</li>
	<li>Ita ne hoc quidem modo paria peccata sunt.</li>
	<li>Si enim, ut mihi quidem videtur, non explet bona naturae voluptas, iure praetermissa est;</li>
	<li>Itaque primos congressus copulationesque et consuetudinum instituendarum voluntates fieri propter voluptatem;</li>
	<li>Cur ipse Pythagoras et Aegyptum lustravit et Persarum magos adiit?</li>
	<li>Cum id fugiunt, re eadem defendunt, quae Peripatetici, verba.</li>
</ul>


<p>An quae de prudentia, de cognitione rerum, de coniunctione generis humani, quaeque ab eisdem de temperantia, de modestia, de magnitudine animi, de omni honestate dicuntur? Hoc autem loco tantum explicemus haec honesta, quae dico, praeterquam quod nosmet ipsos diligamus, praeterea suapte natura per se esse expetenda. Si enim non fuit eorum iudicii, nihilo magis hoc non addito illud est iudicatum-. <a href='http://loripsum.net/' target='_blank'>Quamquam haec quidem praeposita recte et reiecta dicere licebit.</a> Quid in isto egregio tuo officio et tanta fide-sic enim existimo-ad corpus refers? An quod ita callida est, ut optime possit architectari voluptates? Themistocles quidem, cum ei Simonides an quis alius artem memoriae polliceretur, Oblivionis, inquit, mallem. Cum autem venissemus in Academiae non sine causa nobilitata spatia, solitudo erat ea, quam volueramus. Nam hunc ipsum sive finem sive extremum sive ultimum definiebas id esse, quo omnia, quae recte fierent, referrentur neque id ipsum usquam referretur. Aliam vero vim voluptatis esse, aliam nihil dolendi, nisi valde pertinax fueris, concedas necesse est. An nisi populari fama? </p>

<blockquote cite='http://loripsum.net'>
	Ex quo illud efficitur, qui bene cenent omnis libenter cenare, qui libenter, non continuo bene.
</blockquote>


<ol>
	<li>Parvi enim primo ortu sic iacent, tamquam omnino sine animo sint.</li>
	<li>Quo invento omnis ab eo quasi capite de summo bono et malo disputatio ducitur.</li>
	<li>Prodest, inquit, mihi eo esse animo.</li>
	<li>Res enim se praeclare habebat, et quidem in utraque parte.</li>
</ol>


<p>Si enim non fuit eorum iudicii, nihilo magis hoc non addito illud est iudicatum-. <b>Velut ego nunc moveor.</b> Ego vero volo in virtute vim esse quam maximam; Quo studio cum satiari non possint, omnium ceterarum rerum obliti níhil abiectum, nihil humile cogitant; Nullis enim partitionibus, nullis definitionibus utuntur ipsique dicunt ea se modo probare, quibus natura tacita adsentiatur. Quis enim confidit semper sibi illud stabile et firmum permansurum, quod fragile et caducum sit? Si in ipso corpore multa voluptati praeponenda sunt, ut vires, valitudo, velocitas, pulchritudo, quid tandem in animis censes? Cum id quoque, ut cupiebat, audivisset, evelli iussit eam, qua erat transfixus, hastam. <mark>Si longus, levis dictata sunt.</mark> </p>

<p><a href='http://loripsum.net/' target='_blank'>Duarum enim vitarum nobis erunt instituta capienda.</a> Consequatur summas voluptates non modo parvo, sed per me nihilo, si potest; Bona autem corporis huic sunt, quod posterius posui, similiora. An ea, quae per vinitorem antea consequebatur, per se ipsa curabit? Ergo, si semel tristior effectus est, hilara vita amissa est? </p>

<p><a href='http://loripsum.net/' target='_blank'>Id Sextilius factum negabat.</a> De malis autem et bonis ab iis animalibus, quae nondum depravata sint, ait optime iudicari. <a href='http://loripsum.net/' target='_blank'>Omnia peccata paria dicitis.</a> <a href='http://loripsum.net/' target='_blank'>Satis est ad hoc responsum.</a> </p>

<p>Nam cum Academicis incerta luctatio est, qui nihil affirmant et quasi desperata cognitione certi id sequi volunt, quodcumque veri simile videatur. Quod autem meum munus dicis non equidem recuso, sed te adiungo socium. Ex quo intellegitur idem illud, solum bonum esse, quod honestum sit, idque esse beate vivere: honeste, id est cum virtute, vivere. Ego quoque, inquit, didicerim libentius si quid attuleris, quam te reprehenderim. Te enim iudicem aequum puto, modo quae dicat ille bene noris. Honestum igitur id intellegimus, quod tale est, ut detracta omni utilitate sine ullis praemiis fructibusve per se ipsum possit iure laudari. Sunt etiam turpitudines plurimae, quae, nisi honestas natura plurimum valeat, cur non cadant in sapientem non est facile defendere. Quia dolori non voluptas contraria est, sed doloris privatio. Nec vero alia sunt quaerenda contra Carneadeam illam sententiam. <a href='http://loripsum.net/' target='_blank'>Nam ista vestra: Si gravis, brevis;</a> Quaeque de virtutibus dicta sunt, quem ad modum eae semper voluptatibus inhaererent, eadem de amicitia dicenda sunt. <i>Mihi, inquam, qui te id ipsum rogavi?</i> Fortitudinis quaedam praecepta sunt ac paene leges, quae effeminari virum vetant in dolore. Ergo omni animali illud, quod appetiti positum est in eo, quod naturae est accommodatum. </p>

<p>Quodsi Graeci leguntur a Graecis isdem de rebus alia ratione compositis, quid est, cur nostri a nostris non legantur? Curium putes loqui, interdum ita laudat, ut quid praeterea sit bonum neget se posse ne suspicari quidem. Quae fere omnia appellantur uno ingenii nomine, easque virtutes qui habent, ingeniosi vocantur. <i>Haec quo modo conveniant, non sane intellego.</i> Quod mihi quidem visus est, cum sciret, velle tamen confitentem audire Torquatum. Nam et ille apud Trabeam voluptatem animi nimiam laetitiam dicit eandem, quam ille Caecilianus, qui omnibus laetitiis laetum esse se narrat. Te enim iudicem aequum puto, modo quae dicat ille bene noris. Ita similis erit ei finis boni, atque antea fuerat, neque idem tamen; Atque ab his initiis profecti omnium virtutum et originem et progressionem persecuti sunt. Ut enim consuetudo loquitur, id solum dicitur honestum, quod est populari fama gloriosum. Haec bene dicuntur, nec ego repugno, sed inter sese ipsa pugnant. </p>

<p>Ex quo intellegitur idem illud, solum bonum esse, quod honestum sit, idque esse beate vivere: honeste, id est cum virtute, vivere. Tu autem, si tibi illa probabantur, cur non propriis verbis ea tenebas? Tum ille: Tu autem cum ipse tantum librorum habeas, quos hic tandem requiris? <a href='http://loripsum.net/' target='_blank'>Minime vero, inquit ille, consentit.</a> Hunc igitur finem illi tenuerunt, quodque ego pluribus verbis, illi brevius secundum naturam vivere, hoc iis bonorum videbatur extremum. <a href='http://loripsum.net/' target='_blank'>Eam stabilem appellas.</a> Quodsi ipsam honestatem undique pertectam atque absolutam. Sapientem locupletat ipsa natura, cuius divitias Epicurus parabiles esse docuit. Sed quid attinet de rebus tam apertis plura requirere? </p>

<p>Sed audiamus ipsum: Compensabatur, inquit, tamen cum his omnibus animi laetitia, quam capiebam memoria rationum inventorumque nostrorum. Num igitur dubium est, quin, si in re ipsa nihil peccatur a superioribus, verbis illi commodius utantur? Si enim ita est, vide ne facinus facias, cum mori suadeas. Aufert enim sensus actionemque tollit omnem. Plane idem, inquit, et maxima quidem, qua fieri nulla maior potest. Quae tamen a te agetur non melior, quam illae sunt, quas interdum optines. Nunc reliqua videamus, nisi aut ad haec, Cato, dicere aliquid vis aut nos iam longiores sumus. Eodem modo is enim tibi nemo dabit, quod, expetendum sit, id esse laudabile. <a href='http://loripsum.net/' target='_blank'>Cur id non ita fit?</a> Non est enim vitium in oratione solum, sed etiam in moribus. </p>

<p>Satis est tibi in te, satis in legibus, satis in mediocribus amicitiis praesidii. Tum Piso: Atqui, Cicero, inquit, ista studia, si ad imitandos summos viros spectant, ingeniosorum sunt; <a href='http://loripsum.net/' target='_blank'>Eademne, quae restincta siti?</a> An eum locum libenter invisit, ubi Demosthenes et Aeschines inter se decertare soliti sunt? Hic Speusippus, hic Xenocrates, hic eius auditor Polemo, cuius illa ipsa sessio fuit, quam videmus. Quamquam scripsit artem rhetoricam Cleanthes, Chrysippus etiam, sed sic, ut, si quis obmutescere concupierit, nihil aliud legere debeat. </p>

<p>Quo modo autem optimum, si bonum praeterea nullum est? Haec dicuntur inconstantissime. Possumusne hic scire qualis sit, nisi contulerimus inter nos, cum finem bonorum dixerimus, quid finis, quid etiam sit ipsum bonum? <i>Cyrenaici quidem non recusant;</i> Hanc ergo intuens debet institutum illud quasi signum absolvere. <i>Quis istum dolorem timet?</i> Non igitur de improbo, sed de callido improbo quaerimus, qualis Q. Transfer idem ad modestiam vel temperantiam, quae est moderatio cupiditatum rationi oboediens. Eamne rationem igitur sequere, qua tecum ipse et cum tuis utare, profiteri et in medium proferre non audeas? Ita enim se Athenis collocavit, ut sit paene unus ex Atticis, ut id etiam cognomen videatur habiturus. Quid enim tanto opus est instrumento in optimis artibus comparandis? Tu autem negas fortem esse quemquam posse, qui dolorem malum putet. <i>Negat enim summo bono afferre incrementum diem.</i> Atque ego: Scis me, inquam, istud idem sentire, Piso, sed a te opportune facta mentio est. </p>

<p>Antiquorum autem sententiam Antiochus noster mihi videtur persequi diligentissime, quam eandem Aristoteli fuisse et Polemonis docet. Quibus autem in rebus tanta obscuratio non fit, fieri tamen potest, ut id ipsum, quod interest, non sit magnum. <b>Certe non potest.</b> <i>Qui enim existimabit posse se miserum esse beatus non erit.</i> </p>

<ol>
	<li>Nondum autem explanatum satis, erat, quid maxime natura vellet.</li>
	<li>Nihil acciderat ei, quod nollet, nisi quod anulum, quo delectabatur, in mari abiecerat.</li>
	<li>Praetereo multos, in bis doctum hominem et suavem, Hieronymum, quem iam cur Peripateticum appellem nescio.</li>
	<li>Apparet statim, quae sint officia, quae actiones.</li>
	<li>Numquam hoc ita defendit Epicurus neque Metrodorus aut quisquam eorum, qui aut saperet aliquid aut ista didicisset.</li>
	<li>His singulis copiose responderi solet, sed quae perspicua sunt longa esse non debent.</li>
</ol>
'''
