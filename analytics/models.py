from django.db import models
from django.utils.timezone import localtime
from io import StringIO

class Datum(models.Model):
    '''A simple analytics datapoint.'''

    created = models.DateTimeField(null=False, blank=False, default=localtime, db_index=True)
    user_agent = models.TextField(null=False, blank=True, db_index=True)
    referer = models.TextField(null=False, blank=True, db_index=True)
    path = models.TextField(null=False, blank=False, db_index=True)

    def __str__(self):
        with StringIO() as output:
            output.write(f'[{self.created}] ')
            if self.user_agent:
                output.write(f'<{self.user_agent}> ')
            if self.referer:
                output.write(f'{self.referer} => ')
            output.write(self.path)
            return output.getvalue()

    def __repr__(self):
        return f'<Datum {self}>'

