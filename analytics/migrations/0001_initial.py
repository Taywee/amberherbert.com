# Generated by Django 3.2.4 on 2021-06-16 05:50

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Datum',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(db_index=True, default=django.utils.timezone.localtime)),
                ('user_agent', models.TextField(blank=True, db_index=True)),
                ('referer', models.TextField(blank=True, db_index=True)),
                ('path', models.TextField(db_index=True)),
            ],
        ),
    ]
