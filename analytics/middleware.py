from .models import Datum
from datetime import timedelta

class Analytics:
    '''Simple privacy-respecting middleware to store simple analytics information.

    This does not do any tracking, and therefore it ignores DNT.

    Authenticated users are filtered out, but that is after the response is
    retrieved, so the order isn't important.
    '''

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)

        if request.user.is_anonymous and 200 <= response.status_code < 300:
            Datum.objects.create(
                user_agent=request.headers.get('User-Agent', ''),
                referer=request.headers.get('Referer', ''),
                path=request.get_full_path(),
            )

        return response

