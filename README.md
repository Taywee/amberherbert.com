# amberherbert.com

This is the code for Amber Herbert's personal author website.


# Development setup

```sh
$ python3 -mvenv venv
$ . ./venv/bin/activate
$ pip install -r requirements.txt
$ ./manage.py migrate
$ ./manage.py buildsamplestartsetup
```

This will run all the migrations and create a superuser as `dev` with a password
of `dev`, as well as load everything up with some sample starting pages and
styles.  You can then use the standard `./manage.py runserver` to run the
development server locally and access the `/admin` endpoint to get into the
wagtail admin area.

# Copyright

The contents of the site are Copyright 2021-2023 Amber Herbert, except where
otherwise stated for other media content.

The code of this site is Copyright 2021-2023 Taylor C. Richberger.  It is made
available under the GNU GPL Version 3, available in the LICENSE file.

The design is Copyright 2021-2023 Taylor C. Richberger and Amber Herbert.  It is made
available under the Creative Commons Attribution-ShareAlike 4.0 International
license (CC BY-SA).  This means that you may use the design of this site to make
derivative sites, or even use it for commercial purposes, but you must always
release your derivative work under the same license (or one compatible with CC
BY-SA).  The terms of this license are included in the DESIGNLICENSE file.

